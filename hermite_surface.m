P00 = [0,0,0];
P01 = [0,1,0];
P10 = [1,0,0];
P11 = [1,1,0];

P00u = [0,0,1];
P01u = [0,0,1];
P10u = [0,0,-1];
P11u = [0,0,-1];

P00w = [0,0,1];
P01w = [0,0,-1];
P10w = [0,0,1];
P11w = [0,0,-1];

P00uw = [33,0,0];
P01uw = [0,0,0];
P10uw = [0,0,0];
P11uw = [0,0,0];

Patch = []
count = 1
for u=0:0.01:1
    for w=0:0.01:1
       H = [2,-2,1,1;
            -3,3,-2,-1;
            0,0,1,0;
            1,0,0,0];
       F1 = F(w)*H(:,1)*(F(u)*H(:,1)*P00 + F(u)*H(:,2)*P10+...
              F(u)*H(:,3)*P00u + F(u)*H(:,4)*P10u);
          
       F2 =  F(w)*H(:,2)*(F(u)*H(:,1)*P01 + F(u)*H(:,2)*P11+...
              F(u)*H(:,3)*P01u + F(u)*H(:,4)*P11u);
          
       F3 =  F(w)*H(:,3)*(F(u)*H(:,1)*P00w + F(u)*H(:,2)*P10w+...
              F(u)*H(:,3)*P00uw + F(u)*H(:,4)*P10uw);
          
       F4 =  F(w)*H(:,4)*(F(u)*H(:,1)*P01w + F(u)*H(:,2)*P11w+...
              F(u)*H(:,3)*P01uw + F(u)*H(:,4)*P11uw);   
       
       Patch(count,:) = F1 + F2 + F3 + F4;
       count = count+1;
       
      
       % [P00,P01,P00w,P01w;
       %       P10,P11,P10w,P11w;
       %       P00u,P01u,P00uw,P01uw;
       %       P10u,P11u,P10uw,P11uw]
       
    end
    

end
    
    %mesh([Patch(:,1),Patch(:,2),Patch(:,3)])
    scatter3(Patch(:,1),Patch(:,2),Patch(:,3))
    %surf(Patch)
    %plot3(Patch(:,1),Patch(:,2),Patch(:,3),'*')