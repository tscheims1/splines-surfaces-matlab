clear;clc;close all;

p0 = [0,0,0];
p1 = [1,1,0];
p2 = [0,2,0];
p3 = [-1,1,0];
p0t = [1,0,0];
p1t = [0,1,0];
p2t = [-1,0,0];
p3t = [0,-1,0];


H = [2,-2,1,1;
    -3,3,-2,-1;
    0,0,1,0;
    1,0,0,0];


for t=0:0.2:1
    fprintf('curve 0, t=%d',t);
    F(t)*H*[p0;p1;p0t;p1t]
    fprintf('curve 1, t=%d',t);
    F(t)*H*[p1;p2;p1t;p2t]
    fprintf('curve 2, t=%d',t);
    F(t)*H*[p2;p3;p2t;p3t]
end



