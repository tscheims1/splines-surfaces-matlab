

for t=0:0.1:1
    plot(t,2*t^3-3*t^2+1,'b*')
    hold on;
    plot(t,-2*t^3+3*t^2,'g*')
    plot(t,t^3-2*t^2+t,'r*')
    plot(t,t^3-t^2,'k*')
end
hold off;
    